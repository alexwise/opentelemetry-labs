# Environment Set Up

We'll be using a few things to get us started quickly playing with OpenTelemetry. 

First, we'll be doing all of the labs in Google CloudShell. Google provides a standardized Linux environment at no cost.
This will allow us to all start from the same place, with uniform, clean environments to play in.

Second, we'll be using a tool called `otel-cli` to create traces from bash commands.
Usually, the systems that you want to instrument with OpenTelemetry are applications written in "real" programming languages.
In order to keep this class language-agnostic, we're going to be running simple bash commands and showing what they look like as OpenTelemetry trace data.

# Bash and CloudShell

*By the end of this lab, you will:*
1. Have Google Cloud Shell access
1. Be familiar with the Bash commands we'll use in this course
1. Have a "local" development environment set up

---

### Cloud Shell

Log in to [Google Cloud Console](https://console.cloud.google.com/)

Click the CloudShell icon

![image](gcp_menu.png?)

This will open a shell session in your browser.  The rest of this lab will be completed in the Cloud Shell.

Note: If you've used GCP Cloud Shell before, you may have some crusty leftover config in there.  We can probably troubleshoot through it, but you might just want to [reset your Cloud Shell](https://cloud.google.com/shell/docs/resetting-cloud-shell) and start fresh.

---

### Filesystem, Directories, and Navigation

##### Make a new directory to house your coursework

```shell
mkdir ~/sfs
cd ~/sfs/

```

*Explanation:*
- `mkdir` is the command to make a new directory
- `~` is shorthand for your home directory: `/home/your_user_name`
- `sfs` is the new directory we're creating.  `~/sfs` is the full path of the new directory.  It extrapolates to `/home/your_user_name/sfs`
- `cd` is the command to Change our shell into that Directory once it is created.
---

### Installing the Software

#### otel-cli

```shell
git clone https://github.com/equinix-labs/otel-cli.git
cd otel-cli
go build
ls -lah
```

*Explanation:*
- `git clone` will pull down the contents of the [otel-cli repository](https://github.com/equinix-labs/otel-cli) 
- `go build` will compile a go application based on the configuration in the current directory. Google Web Shell comes with Go preconfigured.
- `ls -lah` will list the files in the current directory. You should see the `otel-cli` binary your `go build` command created.

#### uuidgen

The scripts we'll be using with otel-cli have a dependency on `uuidgen` to generate unique dummy data. This dependency does not come preinstalled on the GCP Cloud Shell environment.

```shell
sudo apt install uuid-runtime
```

*Explanation:*
- `sudo` elevates your privileges to superuser for the command. This is used to perform administrative tasks like installing software.
- `apt install` is the package manager on debian-based systems, like Google WebShell
- `uuid-runtime` is the name of the package that otel-cli needs

---

### Standing up Jaeger

[Jaeger](https://www.jaegertracing.io/) is Free, Open-Source distributed tracing console originally built by Uber. While it doesn't have the analytics capabilities of some of the vendor solutions, it is easy to set up and will allow us to visualize our spans.

The otel-cli repository comes with a docker-compose file to run an OpenTelemetry Collector and Jaeger Server.

```shell
docker-compose up
```

*Explanation:*
- You can inspect the contents of the docker-compose file with `cat docker-compose.yaml`.
The file will tell Docker Compose to stand up an [Opentelemetry Collector](https://opentelemetry.io/docs/collector/) listening on port 4317
and a Jaeger server listening on several ports, but the UI is served on 16686. There are many
ways to install and run these two components that are outside the scope of this course.

#### Verifying Jaeger is running

This Jaeger installation runs by default on port 16686, so let's go have a look.

Click on the Web Preview icon in the upper right, and select "Change Port."

![image](web_preview.png?)

Enter "16686" and select "Change and Preview." This should open a new window showing the Jaeger UI.

We haven't sent it any trace data yet, so there's nothing much for us to see here.

---

#### Open and new terminal tab.

In your Cloud Shell browser window, you'll see that our Docker containers have taken over our terminal. We'll need to open a new one.

Click on the "+" sign at the top of the terminal area to open a new terminal tab.

![image](new_terminal.png?)

We'll also need to tell this terminal where our OpenTelemetry collector is listening. We'll do this by setting an environment variable:

```shell
export  OTEL_EXPORTER_OTLP_ENDPOINT=localhost:4317
cd ~/sfs/otel-cli/
```

#### Test that Data is Flowing End-to-End

We're now ready to use otel-cli to test our collector and Jaeger installation.

```shell
./otel-cli exec -n smoke-test -s test-step echo 'hello world'
```

Click back to your Jaeger UI browser tab, and refresh the page. You should now see your "smoke-test" service in the "services" drop-down field.

![image](smoke_test.png?)

Select that service and "Find Traces." It should return a result of 1 span. Click on it.

Congratulations, you've just set up a working OpenTelemetry system!

*Explanation:*
- `./otel-cli` runs the otel-cli binary in your sfs/otel-cli directory
- `exec` tells otel-cli to execute the following shell command
- `-n smoke-test` tells otel-cli the name of the service we want to create the span under. We're calling this "smoke-test"
- `-s test-step` tells otel-cli the name of the step for the command. We're calling it "test-step"
