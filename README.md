# O is for Observability with OpenTelemetry

### Description
Welcome! You have discovered the class materials for the O is for Observability class. I hope you find them helpful in exploring one of the most exciting projects in Open Source


### References:
- Original Meetup link: https://www.meetup.com/sofreeus/events/282771768/
- SFS Mattermost link: https://mattermost.sofree.us/sfs303/channels/o-is-for-observability
- Google Cloud WebShell: https://console.cloud.google.com/getting-started
- Google Slides: https://docs.google.com/presentation/d/1YJm9oWq137k9V35mzJwUwZbYMKteeiRl_zdbPjMAZ8E/edit?usp=sharing
- OpenTelemetry Specification: https://github.com/open-telemetry/opentelemetry-specification
- Otel-cli repository: https://github.com/equinix-labs/otel-cli


#### What this class will teach

- What the OpenTelemetry Project is and why you should learn about it
- How the OpenTelemetry specifications and protocols work at a high level
- Hands-on experience creating spans in Jaeger


#### What this class won't teach

- How to instrument applications for OpenTelemetry in specific languages
- How to build production-grade OpenTelemetry systems.
- Vendor OpenTelemetry solution


## Authors and acknowledgment
Big appreciation to Amy Tobey for creating otel-cli,
And to all the OpenTelemetry maintainers and content-creators.

## License
All materials in this repository are license CC-by-SA Attribution-ShareAlike 2.0: https://creativecommons.org/licenses/by-sa/2.0/legalcode
