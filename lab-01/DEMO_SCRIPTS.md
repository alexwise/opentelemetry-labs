# Using the otel-cli Demo Scripts (Pt 1)

### Messing around

We got to see `./otel-cli exec -n smoke-test -s test-step echo 'hello world'` working at the end of the last lab, so let's play with that some more.

What happens if we swap out the command for something else?

```bash
./otel-cli exec -n "google test" -s "curl" curl "google.com"
```

What happens if we change some of these parameters?

```bash
./otel-cli exec -n "cnn test" -s "$0" curl "cnn.com"
```

*Explanation:*
- `-s "$0"` The $0 variable is a special bash variable that is always set to the command that is being run. this is from the naked shell, so it should return "bash".

### Make the Demo Scripts Executable

`otel-cli` comes with some wonderful demo scripts to get us started creating interesting tracing data. Unfortunately, they aren't executable when you download them. Let's fix that.

```shell
cd ~/sfs/otel-cli/demos
ls -lah
chmod +x *.sh
```

*Explanation:*
- `cd` navigates to the directory where the demo scripts are stored
- `ls` lists all the scripts in that directory
- `chmod` changes the attributes of the files it is told to run against
- `+x` adds the "executable" attribute to those files
- `*.sh` is a regular expression that tells chmod to run against any file in the directory that ends in ".sh"

---

### Let's take a look at some of these scripts...

#### 01-simple-span.sh

The first demo script we're going to run is called "01-simple-span.sh". Let's see what's in it.
There are several ways to take a look at these files, you can open them in the default terminal editor (vim)

```shell
view 01-simple-span.sh
```

Or select the "Open Editor..." button to open the Web IDE, navigate to `~/sfs/otel-cli/demos` and click "Open".

Either way, this is what you should see:

`01-simple-span.sh`
```shell
#!/bin/bash
# an otel-cli demo

# this isn't super precise because of process timing but good enough
# in many cases to be useful
st=$(date +%s.%N) # unix epoch time with nanoseconds
data1=$(uuidgen)
data2=$(uuidgen)
et=$(date +%s.%N)

# don't worry, there are also short options :)
../otel-cli span \
	--service  "demo.sh"     \
	--name     "hello world" \
	--kind     "client"      \
	--start    $st           \
	--end      $et           \
	--tp-print               \
	--attrs "my.data1=$data1,my.data2=$data2"

```

*Explanation:*
- `#!/bin/bash` The shebang, or `#!`, tells the system which interpreter to use to understand this script. This is a bash script, so the interpreter is bash.
- `st=$(date +%s.%N)` Amy, the author, is mocking time here for the purposes of a simple demo. This is getting the time in nanoseconds from the system and saving it to an `$st` variable.
- `data1=$(uuidgen)` This command is creating dummy Universally-Unique IDs add as dummy context data, but we could set these to anything we want.
- `../otel-cli span` The `span` subcommand tells otel-cli to create a span event (part of our tracing) with the following configuration.
- `--service` names the service, like we did for the `smoke-test`
- `--name` names the span event
- `--kind` tells the penTelemetry Collector that this event is coming from a `client`, and may be talking to a server.
- `--start`/`--end` feeds our mocked timestamps as the start and end timing for the span
- `tp-print` tells otel-cli to print out the UUID of the "traceparent" to the terminal so we can see it. Don't worry, we'll discuss traceparents more in the next lab.
- `--attrs "my.data1=$data1,my.data2=$data2"` [OpenTelemetry attributes](https://opentelemetry.lightstep.com/best-practices/using-attributes/) are just key-value pairs of any information we want to attach to this span. We could change it to anything we want.

Let's run it!

```shell
01-simple-span.sh
```
```
# trace id: 09d027a48dcc640d67749dcf805780ca
#  span id: 03586af9edac0fc3
TRACEPARENT=00-09d027a48dcc640d67749dcf805780ca-03586af9edac0fc3-01
```

Switch back to your Jaeger UI window and refresh the page. You should see a new "demo.sh" service in the Services drop-down. Let's search for our traces.

Clicking into the span should pull up all the context we sent it--the UUIDs for `$data1` and `$data2`, as well as the `span.kind=client`

And our timings are fake but we can see them in the span bar.


##### Making it Our Own

If the `attrs` parameters can really be anything we want them to be, so let's make them something that might be useful while debugging.
Let's modify the script like so:

```shell
#!/bin/bash
# an otel-cli demo

# this isn't super precise because of process timing but good enough
# in many cases to be useful
st=$(date +%s.%N) # unix epoch time with nanoseconds


et=$(date +%s.%N)

# don't worry, there are also short options :)
../otel-cli span \
	--service  "demo2.sh"     \
	--name     "hello world" \
	--kind     "client"      \
	--start    $st           \
	--end      $et           \
	--tp-print               \
    --attrs "region=us-east-1,customer-tier=platinum"
```

Now, let's run it again...

```shell
01-simple-span.sh
```

And refresh our Jaeger instance once more, and we can see our new data there. This context is referred to as "baggage" in the OpenTelemetry spec. What other types of information might be useful here?


#### 05-nested-exec.sh

The next demo script is a little more complex, let's break it down.

`05-nested-exec.sh`
```shell
#!/bin/bash
# an otel-cli demo of nested exec
#
# this isn't necessarily practical, but it demonstrates how the TRACEPARENT
# environment variable carries the context from invocation to invocation
# so that the tracing provider (e.g. Honeycomb) can put it all back together

# generate a new trace & span, cli will print out the 'export TRACEPARENT'
carrier=$(mktemp)
../otel-cli span -s $0 -n "traceparent demo" --tp-print --tp-carrier $carrier

# this will start a child span, and run another otel-cli as its program
../otel-cli exec \
	--service    "fake-client" \
	--name       "hammer the server for sweet sweet data" \
	--kind       "client" \
	--tp-carrier $carrier \
	"../otel-cli exec -n fake-server -s 'put up with the clients nonsense' -k server echo 500 NOPE"
	# ^ child span, the responding "server" that just echos NOPE
```

*Explanation:*
- `carrier=$(mktemp)` Create a temporary file to aggregate our context info in. This file is acting as what's known as a [Carrier](https://github.com/open-telemetry/opentelemetry-specification/blob/main/specification/context/api-propagators.md#carrier) in the OpenTelemetry world.
-  `otel-cli span` We saw this in the last script. This creates a span to send to the OpenTelemetry Collector
- `-s $0` name the span after the command that ran the script, so `05-nested-exec.sh`
- `../otel-cli exec` We used this for our initial smoke test, it will run the given command and create a span for it. This will pretend to be a client.
- `../../otel-cli exec -n fake-server...` Our `otel-cli exec` command is running *another* `otel-cli exec` command! They will be part of the same Traceparent because the `$carrier` is the same.
- `echo 500 NOPE` this second `otel-cli exec` command is just echoing `500 NOPE` out to the terminal

Let's see what it does!

```shell
05-nested-exec.sh
```
```
# trace id: cb0916fb0d7e1c1b77631f410a198804
#  span id: ab323c2452d84d59
TRACEPARENT=00-cb0916fb0d7e1c1b77631f410a198804-ab323c2452d84d59-01
500 NOPE
```

Let's see what it looks like in Jaeger...

![image](nested_exec.png?)
